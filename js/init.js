var piqube = piqube || {};

piqube.EXTENSION_ID = chrome.i18n.getMessage('@@extension_id');
piqube.IS_PRODUCTION = false;
piqube.DOMAIN = "https://api.piqube.com/extension";
piqube.bootURL = piqube.DOMAIN + "/can-open-extension";
piqube.getIdURL = piqube.DOMAIN + "/get-request-id";
piqube.postURL = piqube.DOMAIN
piqube.profileURL = piqube.DOMAIN + "/load";

var data = "";
var pageURL = "";

piqube.bootApp = function(){
	var currentURL = window.location.href;

	$.ajax({
		url: piqube.bootURL,
		data: "url="+currentURL,
		async: false,
		success: function(responseText) {
			if(responseText == "true") {
				piqube.getId();

			}
		}
	});

};

piqube.bootApp.history = function(){
	var currentURL = window.location.href;
	$.ajax({
		url: piqube.bootURL,
		data: "url="+currentURL,
		async: false,
		success: function(responseText) {
			if(responseText == "true") {
				piqube.getId.history();

			}
		}
	});

};
piqube.getId = function() {
	$.ajax({
	    url : piqube.getIdURL,
	    async: false,
	    success : function(responseId) {
	    	piqube.uploadData(responseId);
	    }
	  });
};

piqube.getId.history = function() {
	$.ajax({
	    url : piqube.getIdURL,
	    async: true,
	    success : function(responseId) {
	    	piqube.uploadData(responseId);
	    }
	  });
};


piqube.uploadData = function(id) {
	var currentURL = window.location.href;
	var formData = new FormData();
	formData.append('file',data);
	formData.append('url',encodeURIComponent(pageURL));

	$.ajax({
		url: piqube.postURL+"/"+id+"/upload",
		type: "POST",
  		contentType: false,
        processData: false,
        data: formData,
		success: function(res) {
			if(res == "true") {
			if(currentURL.indexOf("dice.com") > -1 ||currentURL.indexOf("wisdomjobs.com") > -1 || currentURL.indexOf("jobsdb") > -1 || currentURL.indexOf("jobstreet") > -1){
            			   return;
            			   }
            			   else{
				piqube.initFrame(id);
				}
			}
		}

	});

};

piqube.initFrame = function(id) {

	var DIV = document.createElement("div");
	DIV.style.position = "fixed";
	//DIV.style.height = "100%";
	DIV.style.width = "0";
	DIV.style.top = "0";
	DIV.style.right = "0";
	DIV.style.zIndex = "3147483647";
	DIV.style.background = "#FFF";
	DIV.style.borderLeft = "1px solid #CCC";
	DIV.setAttribute("id","piqube-con");

	var iFrame  = document.createElement ("iframe");
	console.log("page url:"+pageURL);
	console.log("request id:"+id);

	extension_profile_url  = piqube.profileURL + "?requestid="+id+"&url="+pageURL;
	iFrame.src = chrome.runtime.getURL('src/loader.html?ex_pr_url='+extension_profile_url);
	iFrame.style.width = "280px";
	iFrame.style.height = "20%";
	iFrame.setAttribute("id","piqube-container");

	var DIVTOGGLE = document.createElement("div");
	DIVTOGGLE.style.position = "absolute";
	DIVTOGGLE.style.top = "50%";
	DIVTOGGLE.style.left = "-40px";
	DIVTOGGLE.setAttribute("id","piqube-con-toggle");
	DIVTOGGLE.style.position = "absolute";

	var iToggle = document.createElement("i");
	iToggle.style.height = "40px";
	iToggle.style.width = "40px";
	iToggle.style.display = "block";
	iToggle.style.cursor = "pointer";
	var toggle_out = chrome.runtime.getURL('icons/toggle_out.png');
	iToggle.style.backgroundImage = "url("+toggle_out+")";

	$("body").append(DIV);
	$("#piqube-con").append(iFrame);
	$("#piqube-con").append(DIVTOGGLE);
	$("#piqube-con-toggle").append(iToggle);
	piqube.initToggle();
};

piqube.initToggle = function() {
	$("body").css("transition","margin-right 0.5s ease-in-out");
	$("#top-header").css("transition","margin-right 0.5s ease-in-out");
	$("#piqube-con").css("transition","all 0.5s ease-in-out");
	$("#piqube-con-toggle").click(function(){ piqube.toggle();	});
	if($.cookie('toggle')=="false") { piqube.toggleIn(); }
	else { piqube.toggleOut(); }
};

piqube.toggleOut = function() {
	var toggle_in = chrome.runtime.getURL('icons/toggle_in.png');
	$("#piqube-con-toggle").addClass("toggled");
	$("body").css("margin-right","280px");
	$("#piqube-con").css("width","280px");
	$("#top-header").css("margin-right","280px");
	$("#piqube-con-toggle i").css('background-image',"url("+toggle_in+")");
};

piqube.toggleIn = function() {
	var toggle_out = chrome.runtime.getURL('icons/toggle_out.png');
	$("body").css("margin-right","0");
	$("#top-header").css("margin-right","0");
	$("#piqube-con").css("width","0px");
	$("#piqube-con-toggle i").css('background-image',"url("+toggle_out+")");
	$("#piqube-con-toggle").removeClass("toggled");

};

piqube.toggle = function() {

	if($("#piqube-con-toggle").hasClass("toggled")) {

		piqube.toggleIn();
		$.cookie('toggle','false');
	}
	else if($.cookie('toggle') == "false") {

		piqube.toggleOut();
		$.cookie('toggle','true');
	}

	else if($.cookie('toggle') == "true") {
		piqube.toggleOut();
	}

};


$('document').ready(function(){

	data = $("html").html();
	pageURL = window.location.href;
	piqube.bootApp();

	//for twitter page other profile links
	if (pageURL.indexOf("twitter") !=-1) {
	$(document).on("click","a.ProfileTweet-originalAuthorLink",function(){

            var hrefTest=$(this).attr('href');
            data = $("html").html();
            //causes the page to refresh
           	window.location.href = location.href.substring(0,location.href.lastIndexOf("/"))+hrefTest;
           	pageURL=window.location.href;
           	piqube.bootApp();


           });
    }

    //refer-candidate
    $("#refer-candidate").on('click',function(){
    console.log("Refer this candidate is called::::");
    		var jobId = $("#job-id").val();
    		console.log(jobId);

    		/*$.ajax({
    			url: "",
    			data: "data="+feedback,
    			success: function(data){
    				if(data != "true") {
    					$("#feedback-text").fadeOut();
    					$("#send-feedback").fadeOut();
    					$(".feedback-container").append("<span>Thanks for your feedback.</span>");

    				}
    			}
    		});*/

    	});



});


//to load extension when browser's back button is clicked
$(window).unload( function ()
{
    data = $("html").html();
    pageURL = window.location.href;
    piqube.bootApp.history();
});
//Adding Notification
/*
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    console.log(sender.tab ?
                "from a content script:" + sender.tab.url :
                "from the extension");
    if (request.message == "notify") {
     if($.cookie('toggle') == "false"){

     	}
    }
  });*/

