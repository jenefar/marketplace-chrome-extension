var getURLParameter = function(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '(.*)').exec(location.search)||[,""])[1].replace(/\+/g, '%20')) || null;
}

var iframe = document.createElement('iframe');
iframe.setAttribute('src', getURLParameter('ex_pr_url'));
iframe.setAttribute('frameBorder', '0');
document.body.appendChild(iframe);


var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
var eventer = window[eventMethod];
var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

// Listen to message from child iframe
eventer(messageEvent,function(e) {
    if(e.message == "loggedIn"){
      console.log("USER IS LOGGED IN")
    }
    else {
      console.log("USER IS NOT LOGGED IN");
    }
},false);